package com.zihadrizkyef.aispacewar

import android.graphics.Canvas
import android.graphics.Point
import android.util.Log
import com.zihadrizkyef.aispacewar.gameobject.Bullet
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun checkIntersec() {
        val pointA = Point(0, 0)
        val pointB = Point(100, 0)
        print(pointA.x)
        print(pointB.x)
        val pointCircle = Point(50, 1)
        val radius = 5
        Log.i("AOEU", getCircleLineIntersectionPoint(pointA, pointB, pointCircle, radius.toDouble()).toString())
    }

    fun getCircleLineIntersectionPoint(pointA: Point, pointB: Point, center: Point, radius: Double): List<Point> {
        val baX = pointB.x - pointA.x
        val baY = pointB.y - pointA.y
        val caX = center.x - pointA.x
        val caY = center.y - pointA.y

        val a = baX * baX + baY * baY
        val bBy2 = baX * caX + baY * caY
        val c = caX * caX + caY * caY - radius * radius

        val pBy2 = bBy2 / a
        val q = c / a

        val disc = pBy2 * pBy2 - q
        if (disc < 0) {
            return Collections.emptyList()
        }
        // if disc == 0 ... dealt with later
        val tmpSqrt = Math.sqrt(disc)
        val abScalingFactor1 = -pBy2 + tmpSqrt
        val abScalingFactor2 = -pBy2 - tmpSqrt

        val p1 = Point((pointA.x - baX * abScalingFactor1).toInt(), (pointA.y
                - baY * abScalingFactor1).toInt())
        if (disc == 0.0) {
            return Collections.singletonList(p1)
        }
        val p2 = Point((pointA.x - baX * abScalingFactor2).toInt(), (pointA.y
                - baY * abScalingFactor2).toInt())
        return Arrays.asList(p1, p2)
    }
}
