package com.zihadrizkyef.aispacewar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import java.util.*



open class GameRoom @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : SurfaceView(context, attrs, defStyleAttr), SurfaceHolder.Callback {

    lateinit var thread: GameThread
    val random = Random()
    val instanceList = ArrayList<GameObject>()
    private val instanceToRemove = ArrayList<GameObject>()

    init {
        holder.addCallback(this)
        isFocusable = true
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        thread = GameThread(this, holder!!)
        thread.running = true
        thread.start()

        create()
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

    }
    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        Log.i("AOEU", "Surface Destroyed")
        thread.running = false
        thread.join()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        instanceList.forEach { it.onTouchEvent(event) }
        return true
    }

    open fun create() {}

    open fun update() {
        val length = instanceList.size
        for (i in 0 until length) {
            if (instanceList[i].alive) {
                instanceList[i].onUpdate()
            }
        }
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        canvas?.drawColor(Color.parseColor("#111111"))

        val tempList = instanceList.sortedWith(compareByDescending { it.drawDepth })
        tempList.forEach {
            if (it.alive) {
                it.onBottomDraw(canvas)
            }
        }
        tempList.forEach {
            if (it.alive) {
                it.onDraw(canvas)
            }
        }
        tempList.forEach {
            if (it.alive) {
                it.onTopDraw(canvas)
            }
        }

        instanceToRemove.forEach { it.onDestroy() }
        instanceList.removeAll(instanceToRemove)
        instanceToRemove.clear()
    }

    open fun addInstance(instance: GameObject) {
        synchronized(instanceList) {
            instance.alive = true
            instanceList.add(instance)
        }
    }

    open fun removeInstance(instance: GameObject) {
        instance.alive = false
        instanceToRemove.add(instance)
    }
}
