package com.zihadrizkyef.aispacewar

import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import java.util.*
import kotlin.math.cos
import kotlin.math.sin


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 08/10/18.
 */
abstract class GameObject(val context: Context, val room: GameRoom) {
    companion object {
        fun pointDistance(x1: Float, y1: Float, x2: Float, y2: Float): Float {
            return Math.sqrt(Math.pow((x1 - x2).toDouble(), 2.0) + Math.pow((y1 - y2).toDouble(), 2.0)).toFloat()
        }

        fun getCircleLineIntersectionPoint(x1: Double, y1: Double, x2: Double, y2: Double, cx: Double, cy: Double, radius: Double): ArrayList<Function.PointD> {
            return Function().lineSegmentCircleIntersection(Function.LineSegment(x1, y1, x2, y2), Function.Circle(cx, cy, radius))
        }

        fun pointDirection(p1: PointF, p2: PointF): Double {
            val deltaY = (p1.y - p2.y).toDouble()
            val deltaX = (p2.x - p1.x).toDouble()
            val result = Math.toDegrees(Math.atan2(deltaY, deltaX))
            return if (result < 0) 360.0 + result else result
        }

        fun xLengthOfDir(dist: Double, direction: Double): Double {
            return if (direction == 90.0 || direction == 270.0) {
                0.0
            } else {
                dist * cos(Math.toRadians(direction))
            }
        }

        fun yLengthOfDir(dist: Double, direction: Double): Double {
            return if (direction == 180.0) {
                0.0
            } else {
                -dist * sin(Math.toRadians(direction))
            }
        }

        fun distanceAngle(dir1: Double, dir2: Double): Double {
            val phi = Math.abs(dir2 - dir1) % 360
            return if (phi > 180) 360 - phi else phi
        }

        fun signAngle(dir1: Double, dir2: Double): Int {
            return if (dir1 - dir2 in 0.0..180.0 || dir1 - dir2 <= -180 && dir1 - dir2 >= -360) -1 else 1
        }
    }

    private var alarmCounter = arrayListOf<Int>()
    private var alarmLength = arrayListOf<Int>()
    private var alarmAction = arrayListOf<Runnable>()

    private val textBounds = Rect()

    val random = Random()
    var drawDepth = 0
    var paint = Paint().apply { color = Color.BLACK }

    var x = 0F
    var y = 0F
    var alive = false
    var speed = 0.0
    var direction = 0.0
        set(value) {
            field = when {
                value > 360 -> value - 360
                value < 0 -> value + 360
                else -> value
            }
        }

    open fun onUpdate() {
        x += xLengthOfDir(speed, direction).toFloat()
        y += yLengthOfDir(speed, direction).toFloat()
        updateAlarm()
    }

    open fun onTouchEvent(event: MotionEvent?) {}
    open fun onBottomDraw(canvas: Canvas?) {}
    open fun onDraw(canvas: Canvas?) {}
    open fun onTopDraw(canvas: Canvas?) {}
    open fun onDestroy() {}

    protected fun setAlarm(length: Int, action: Runnable): Int {
        alarmCounter.add(0)
        alarmLength.add(length)
        alarmAction.add(action)

        return alarmCounter.size - 1
    }

    protected fun cancelAlarm(id: Int) {
        alarmCounter[id] = alarmLength[id] + 1
    }

    private fun updateAlarm() {
        for (i in 0 until alarmLength.size) {
            if (alarmCounter[i] != -1 && alarmCounter[i] < alarmLength[i]) {
                alarmCounter[i]++
            } else if (alarmCounter[i] == alarmLength[i]) {
                alarmAction[i].run()
                alarmCounter[i]++
            }
        }
    }

    fun <T>choose(vararg value: T): T {
        val rNum = random.nextInt(value.size)
        return value[rNum]
    }
    fun randomIntBetween(min: Int, max: Int): Int {
        return random.nextInt(max + 1 - min) + min
    }

    fun directionTo(point: PointF): Double {
        return pointDirection(PointF(x, y), point)
    }

    fun distanceTo(x: Float, y: Float): Float {
        return pointDistance(this.x, this.y, x, y)
    }

    fun Paint.getTextWidth(text: String): Float {
        var max = 0F
        for (line in text.split("\n")) {
            if (max < measureText(line)) {
                max = measureText(line)
            }
        }
        return max
    }

    fun Paint.getTextHeight(text: String): Float {
        var height = 0F
        for (line in text.split("\n")) {
            val rect = Rect()
            getTextBounds(line, 0, line.length, rect)
            height += rect.height()
        }
        return height
    }

    fun Canvas.drawTextMultiline(x: Float, y: Float, paint: Paint, text: String) {
        var yText = y
        for (line in text.split("\n")) {
            drawText(line, x, yText, paint)

            yText += paint.descent() - paint.ascent()
        }
    }
}