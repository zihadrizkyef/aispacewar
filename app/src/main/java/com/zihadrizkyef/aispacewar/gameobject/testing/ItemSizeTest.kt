package com.zihadrizkyef.aispacewar.gameobject.testing

import android.content.Context
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameobject.item.ItemSize

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 23/10/18.
 */

class ItemSizeTest(context: Context, room: GameRoom) : ItemSize(context, room) {
    init {
        speed = 0.0
        text = "AAA"
    }
}