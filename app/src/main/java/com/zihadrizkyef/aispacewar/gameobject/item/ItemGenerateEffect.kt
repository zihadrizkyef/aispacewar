package com.zihadrizkyef.aispacewar.gameobject.item

import android.content.Context
import android.graphics.Canvas
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/10/18.
 */
class ItemGenerateEffect(context: Context, room: GameRoom) : GameObject(context, room) {
    var size = 0
    var alpha = 1.0

    init {
        drawDepth = 2
    }

    override fun onUpdate() {
        super.onUpdate()

        size += 7
        if (alpha > 0) {
            alpha -= 0.05
        } else {
            room.removeInstance(this)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        paint.alpha = (255 * alpha).toInt()
        canvas?.drawCircle(x, y, (size / 2).toFloat(), paint)
    }

    override fun onDestroy() {

    }
}