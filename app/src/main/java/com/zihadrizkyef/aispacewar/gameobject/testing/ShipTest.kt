package com.zihadrizkyef.aispacewar.gameobject.testing

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.view.MotionEvent
import com.zihadrizkyef.aispacewar.Function
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameobject.item.Item

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 23/10/18.
 */
class ShipTest(context: Context, room: GameRoom) : GameObject(context, room) {
    private val size = 50

    init {
        paint.color = choose(
                Color.CYAN,
                Color.GREEN,
                Color.MAGENTA,
                Color.YELLOW,
                Color.RED
        )
    }

    override fun onTouchEvent(event: MotionEvent?) {
        if (event!!.action == MotionEvent.ACTION_MOVE) {
            x = event.x
            y = event.y - 200
        }
    }

    override fun onUpdate() {
        room.instanceList.asSequence()
                .filter { it is Item }
                .map { it as Item }
                .toList()
                .forEach { item ->
                    val rect = item.rect.copy()
                    val halfWidth = rect.width / 2
                    val halfHeight = rect.height / 2
                    rect.x -= halfWidth
                    rect.y -= halfHeight
                    rect.angle = Math.toRadians(rect.angle)
                    val collide = com.zihadrizkyef.aispacewar.Function().circleRectangleIntersection(rect, Function.Circle(x.toDouble(), y.toDouble(), (size / 2).toDouble()))
                    if (collide) {
                        paint.color = choose(
                                Color.CYAN,
                                Color.GREEN,
                                Color.MAGENTA,
                                Color.YELLOW,
                                Color.RED
                        )
                    }
                }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawCircle(x, y, (size / 2).toFloat(), paint)
    }
}