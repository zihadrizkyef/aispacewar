package com.zihadrizkyef.aispacewar.gameobject

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PointF
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 17/10/18.
 */

class StarBackground(context: Context, room: GameRoom) : GameObject(context, room) {
    private val starList = arrayListOf<Star>()

    private val pointCenter = PointF(room.width / 2F, room.height / 2F)
    private val radiusDiagonal = pointDistance(pointCenter.x, pointCenter.y, 0F, 0F) + 5

    private val generateStarRnbl = object : Runnable {
        override fun run() {
            val dirPlace = direction + 180 - random.nextInt(90) + random.nextInt(90)

            val hsv = FloatArray(3)
            hsv[0] = 0F
            hsv[1] = 0F
            hsv[2] = Math.min(0.75F + random.nextFloat(), 1F)
            val star = Star(
                    pointCenter.x + xLengthOfDir(radiusDiagonal.toDouble(), dirPlace).toFloat(),
                    pointCenter.y + yLengthOfDir(radiusDiagonal.toDouble(), dirPlace).toFloat(),
                    Color.HSVToColor(hsv),
                    random.nextInt(4).toFloat()
            )
            starList.add(star)

            val second = room.thread.MAX_FPS
            setAlarm((second * 0.1).toInt(), this)
        }
    }

    init {
        paint.color = Color.WHITE
        direction = random.nextInt(360).toDouble()
        speed = 3.0
        drawDepth = 100

        for (i in 0..130) {
            val hsv = FloatArray(3)
            hsv[0] = 0F
            hsv[1] = 0F
            hsv[2] = Math.min(0.75F + random.nextFloat(), 1F)
            val star = Star(
                    random.nextInt(room.width).toFloat(),
                    random.nextInt(room.height).toFloat(),
                    Color.HSVToColor(hsv),
                    random.nextInt(4).toFloat()
            )
            starList.add(star)
        }

        val second = room.thread.MAX_FPS
        setAlarm((second * 0.1).toInt(), generateStarRnbl)
    }

    override fun onUpdate() {
        super.onUpdate()

        val removeList = arrayListOf<Star>()
        starList.forEach {
            val distance = pointDistance(it.x, it.y, pointCenter.x, pointCenter.y)
            if (distance > radiusDiagonal) {
                removeList.add(it)
            } else {
                it.x += xLengthOfDir(speed, direction).toFloat()
                it.y += yLengthOfDir(speed, direction).toFloat()
            }
        }
        starList.removeAll(removeList)
        removeList.clear()
    }

    override fun onBottomDraw(canvas: Canvas?) {
        super.onBottomDraw(canvas)

        starList.forEach {
            paint.color = it.color
            canvas?.drawCircle(it.x, it.y, it.size / 2, paint)
        }
    }
}