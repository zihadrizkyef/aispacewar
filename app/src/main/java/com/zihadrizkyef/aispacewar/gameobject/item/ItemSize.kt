package com.zihadrizkyef.aispacewar.gameobject.item

import android.content.Context
import android.graphics.Color
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameobject.Ship

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/10/18.
 */

open class ItemSize(context: Context, room: GameRoom) : Item(context, room) {
    init {
        text = "SIZE"
        paint.color = Color.GREEN
    }

    override fun onShipCollide(ship: Ship) {
        super.onShipCollide(ship)

        ship.size += 5
    }
}