package com.zihadrizkyef.aispacewar.gameobject

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import com.zihadrizkyef.aispacewar.Function
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameroom.WarLand
import kotlin.math.min


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 08/10/18.
 */
class Ship(context: Context, room: GameRoom) : GameObject(context, room) {
    private val paintText = Paint().apply { color = Color.WHITE }

    private var avoidingBullet = false
    private var avoidingEnemy = false
    private var targetDirection = 0.0
    private var fireCount = 0
    private var insideWarLand = false
    val minSize = 20
    var size = minSize
    var fireMax = 5.0
    private var fireMaxTarget = random.nextInt(fireMax.toInt())
    var fireDelay = 10
    var color = Color.WHITE

    private val changeDirRnbl = object : Runnable {
        override fun run() {
            if (!avoidingEnemy && !avoidingBullet && insideWarLand) {
                targetDirection = random.nextInt(360).toDouble()
            }

            val second = room.thread.MAX_FPS
            setAlarm(random.nextInt(second), this)
        }
    }
    private val generateBulletRnbl = object : Runnable {
        override fun run() {
            //Choosing enemy to kill
            val enemyList = (room as WarLand).shipList.filter { it != this@Ship && it.color != color }
            if (enemyList.isNotEmpty()) {
                val shipBig = enemyList.filter { it.size >= 70 }
                val shipReallyBig = enemyList.filter { it.size >= 100 }
                val shipNear = enemyList.filter { distanceTo(it.x, it.y) < room.width / 2.5 }
                val shipTarget = when {
                    shipReallyBig.isNotEmpty() -> shipReallyBig[random.nextInt(shipReallyBig.size)]
                    shipNear.isNotEmpty() -> shipNear.sortedWith(compareBy { distanceTo(it.x, it.y) })[0]
                    shipBig.isNotEmpty() -> shipBig[random.nextInt(shipBig.size)]
                    else -> enemyList[random.nextInt(enemyList.size)]
                }

                val bullet = Bullet(this@Ship, context, room)
                bullet.x = (x + xLengthOfDir((size / 2).toDouble(), directionTo(PointF(shipTarget.x, shipTarget.y)))).toFloat()
                bullet.y = (y + yLengthOfDir((size / 2).toDouble(), directionTo(PointF(shipTarget.x, shipTarget.y)))).toFloat()
                bullet.direction = directionTo(PointF(shipTarget.x, shipTarget.y)) + random.nextInt(10) - random.nextInt(10) - random.nextDouble() + random.nextDouble()
                x -= xLengthOfDir(size / 12.0, bullet.direction).toFloat()
                y -= xLengthOfDir(size / 12.0, bullet.direction).toFloat()
                room.addInstance(bullet)

                fireCount++
            }

            if (fireCount < fireMaxTarget) {
                setAlarm(fireDelay, this)
            } else {
                fireCount = 0
                fireMaxTarget = random.nextInt(fireMax.toInt())
                val second = room.thread.MAX_FPS
                setAlarm((second * 0.5 + random.nextInt(second)).toInt(), this)
            }
        }
    }
    private val changeColorRnbl = object : Runnable {
        override fun run() {
            color = choose(
                    Color.CYAN,
                    Color.GREEN,
                    Color.MAGENTA,
                    Color.YELLOW,
                    Color.RED
            )

            if (size >= 100) {
                val second = room.thread.MAX_FPS
                setAlarm((second * 0.05).toInt(), this)
            }
        }
    }

    init {
        speed = 3.0
        direction = random.nextInt(360).toDouble()

        val second = room.thread.MAX_FPS
        setAlarm(random.nextInt(second), changeDirRnbl)
        setAlarm(random.nextInt((second * 0.5).toInt()), generateBulletRnbl)
    }

    override fun onUpdate() {
        super.onUpdate()

        //Getting inside room
        if (!insideWarLand) {
            direction = directionTo(PointF((room.width / 2).toFloat(), (room.height / 2).toFloat())) + random.nextInt(90) - random.nextInt(90)
            if (x - size / 2 > 0 && x + size / 2 < room.width && y - size / 2 > 0 && y + size / 2 < room.height) {
                insideWarLand = true
            }
        } else {

            //Prevent ship to go outside room
            if (x < size / 2) {
                x = (size / 2).toFloat()
                direction = (90 - random.nextInt(180)).toDouble()
            }
            if (x > room.width - size / 2) {
                x = (room.width - size / 2).toFloat()
                direction = (90 + random.nextInt(180)).toDouble()
            }
            if (y < size / 2) {
                y = (size / 2).toFloat()
                direction = (180 + random.nextInt(180)).toDouble()
            }
            if (y > room.height - size / 2) {
                y = ((room.height - size / 2).toFloat())
                direction = random.nextInt(180).toDouble()
            }

            avoidingEnemy = true
            avoidingBullet = true
            val avoidBullet = choose(true, false)

            //Avoid other ship
            (room as WarLand).shipList.filter { it != this }.forEach { enemy ->
                val dist = distanceTo(enemy.x, enemy.y)
                val collideDistance = ((size / 2) + (enemy.size / 2))
                val collide = (dist <= collideDistance)
                if (collide) {
                    val explosion1 = ShipExplosion(this, context, room)
                    room.addInstance(explosion1)
                    room.removeInstance(this)

                    val explosion2 = ShipExplosion(enemy, context, room)
                    room.addInstance(explosion2)
                    room.removeInstance(enemy)
                } else if (dist > collideDistance && dist < collideDistance + 300) {
                    targetDirection = directionTo(PointF(enemy.x, enemy.y)) + 180 + random.nextInt(90) - random.nextInt(90)
                } else {
                    avoidingEnemy = false
                }
            }

            //Avoid bullet
            room.bulletList.forEach { bullet ->
                val awareDistance = 30
                val lineSegment = Function.LineSegment(
                        bullet.x.toDouble(),
                        bullet.y.toDouble(),
                        bullet.x + xLengthOfDir(bullet.speed * awareDistance, bullet.direction),
                        bullet.y + yLengthOfDir(bullet.speed * awareDistance, bullet.direction)
                )
                val circle = Function.Circle(x.toDouble(), y.toDouble(), (size / 2).toDouble())
                val intersect = Function().lineSegmentCircleIntersection(lineSegment, circle)
                if (intersect.isNotEmpty()) {
                    if (avoidBullet) {
                        val distDir1 = distanceAngle(direction, bullet.direction + 90)
                        val distDir2 = distanceAngle(direction, bullet.direction - 90)
                        targetDirection = if (distDir1 < distDir2) {
                            bullet.direction + 90
                        } else {
                            bullet.direction - 90
                        }
                    }
                } else {
                    avoidingBullet = false
                }
            }

            //Chase Item
            if (!avoidingBullet && !avoidingEnemy) {
                val itemList = room.itemList.sortedWith(compareBy { distanceTo(it.x, it.y) })
                if (itemList.isNotEmpty()) {
                    val nearestItem = itemList[0]
                    targetDirection = directionTo(PointF(nearestItem.x, nearestItem.y))

                    itemList.forEach { item ->
                        val rect = item.rect.copy()
                        val halfWidth = rect.width / 2
                        val halfHeight = rect.height / 2
                        rect.x -= halfWidth
                        rect.y -= halfHeight
                        rect.angle = Math.toRadians(rect.angle)
                        val collide = Function().circleRectangleIntersection(item.rect, Function.Circle(x.toDouble(), y.toDouble(), (size / 2).toDouble()))
                        if (collide) {
                            item.onShipCollide(this)
                        }
                    }
                }
            }
        }

        val stirSpeed = distanceAngle(direction, targetDirection) / 10
        direction += stirSpeed * signAngle(direction, targetDirection)
    }

    // Return true if killed by bullet else return false
    fun onCollisionWithBullet(bullet: Bullet): Boolean {
        size -= bullet.ship.size / 3
        when {
            size <= minSize -> {
                room.removeInstance(this)
                val explosion = ShipExplosion(this, context, room)
                room.addInstance(explosion)

                return true
            }
            else -> {
                val bulletImpact = BulletImpact(bullet, context, room)
                room.addInstance(bulletImpact)
            }
        }

        if (speed > 3 + 2) {
            speed -= 2
        }

        if (fireMax > 10) {
            fireMax -= 0.3
        }

        if (fireDelay < room.thread.MAX_FPS) {
            fireDelay += 1
        }

        return false
    }

    fun onBulletKillEnemy() {
        size = min(size + 5, 130)
        speed = min(speed + 1, 22.0)
        fireMax += 3
        if (fireDelay > room.thread.MAX_FPS * 0.25) {
            fireDelay -= 10
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        paint.color = color
        paint.alpha = 255
        canvas?.drawCircle(x, y, (size / 2).toFloat(), paint)

        if (room.thread.DEBUG) {
            canvas?.drawLine(x, y, (x + xLengthOfDir(30.0, direction)).toFloat(), (y + yLengthOfDir(30.0, direction)).toFloat(), paint)
            canvas?.drawTextMultiline(x + size, y + size, paintText, "x=$x\n" +
                    "y=$y\n" +
                    "dir=$direction\n" +
                    "targetDir=$targetDirection\n" +
                    "spd=$speed\n")
        }
    }
}