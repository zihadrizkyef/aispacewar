package com.zihadrizkyef.aispacewar.gameobject

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 15/10/18.
 */
class BulletImpact(bullet: Bullet, context: Context, room: GameRoom) : GameObject(context, room) {
    private val size = bullet.speed
    private val destroyRnbl = Runnable { room.removeInstance(this@BulletImpact) }

    init {
        x = bullet.x + xLengthOfDir(bullet.speed, bullet.direction).toFloat()
        y = bullet.y + yLengthOfDir(bullet.speed, bullet.direction).toFloat()
        val bulletColor = FloatArray(3)
        Color.colorToHSV(bullet.paint.color, bulletColor)
        bulletColor.set(1, bulletColor[1] - 0.5F)
        paint.color = Color.HSVToColor(bulletColor)

        setAlarm(2, destroyRnbl)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawCircle(x, y, (size / 3).toFloat(), paint)
    }
}