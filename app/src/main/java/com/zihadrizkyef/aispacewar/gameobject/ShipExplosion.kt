package com.zihadrizkyef.aispacewar.gameobject

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 15/10/18.
 */
class ShipExplosion(ship: Ship, context: Context, room: GameRoom) : GameObject(context, room) {
    var visible = true
    var size = ship.size * 1.5

    private val changeVisibilityRnbl = object:Runnable {
        override fun run() {
            visible = !visible

            val second = room.thread.MAX_FPS
            setAlarm((second*0.03).toInt(), this)
        }
    }
    private val destroyRnbl = Runnable { room.removeInstance(this@ShipExplosion) }

    init {
        x = ship.x
        y = ship.y
        val shipColor = FloatArray(3)
        Color.colorToHSV(ship.color, shipColor)
        shipColor.set(1, shipColor[1]-0.5F)
        paint.color = Color.HSVToColor(shipColor)

        val second = room.thread.MAX_FPS
        setAlarm((second * 0.03).toInt(), changeVisibilityRnbl)
        setAlarm(second, destroyRnbl)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (visible) {
            canvas?.drawCircle(x, y, size.toFloat(), paint)
        }
    }
}