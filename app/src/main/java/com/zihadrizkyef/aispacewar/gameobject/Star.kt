package com.zihadrizkyef.aispacewar.gameobject

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/10/18.
 */

data class Star(var x: Float, var y: Float, var color: Int, var size: Float)