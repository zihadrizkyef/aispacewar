package com.zihadrizkyef.aispacewar.gameobject.item

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.support.v4.content.res.ResourcesCompat
import android.util.TypedValue
import com.zihadrizkyef.aispacewar.Function
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.R
import com.zihadrizkyef.aispacewar.gameobject.Ship
import kotlin.math.max


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/10/18.
 */
abstract class Item(context: Context, room: GameRoom) : GameObject(context, room) {
    val rotateDir = choose(-1, 1)
    val rect = Function.Rectangle(0.0, 0.0, 0.0, 0.0, random.nextInt(360).toDouble())
    var text = "ITEM"

    private val changeDirRnbl = object : Runnable {
        override fun run() {
            direction = random.nextInt(360).toDouble()

            val second = room.thread.MAX_FPS
            setAlarm(second + random.nextInt(second * 2), this)
        }
    }

    init {
        paint.typeface = ResourcesCompat.getFont(context, R.font.ubuntumono_bold)
        paint.textAlign = Paint.Align.CENTER
        paint.textSize = 30F
        speed = 1.5
        drawDepth = 1

        val second = room.thread.MAX_FPS
        setAlarm(second + random.nextInt(second * 2), changeDirRnbl)
    }

    open fun onShipCollide(ship: Ship) {
        room.removeInstance(this)
    }

    override fun onUpdate() {
        super.onUpdate()

        //Prevent item to go outside room
        val size = max(rect.width, rect.height)
        if (x < size / 2) {
            x = (size / 2).toFloat()
            direction = (90 - random.nextInt(180)).toDouble()
        }
        if (x > room.width - size / 2) {
            x = (room.width - size / 2).toFloat()
            direction = (90 + random.nextInt(180)).toDouble()
        }
        if (y < size / 2) {
            y = (size / 2).toFloat()
            direction = (180 + random.nextInt(180)).toDouble()
        }
        if (y > room.height - size / 2) {
            y = ((room.height - size / 2).toFloat())
            direction = random.nextInt(180).toDouble()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        rect.angle += rotateDir
        if (rect.angle > 360) {
            rect.angle -= 360
        }
        if (rect.angle < 0) {
            rect.angle += 360
        }
        rect.width = paint.getTextWidth(text).toDouble()
        rect.height = paint.getTextHeight(text).toDouble()
        rect.x = x - rect.width / 2
        rect.y = y - rect.height / 2

        canvas?.save()
        canvas?.translate(rect.x.toFloat(), rect.y.toFloat())
        canvas?.rotate(-rect.angle.toFloat())
        canvas?.drawTextMultiline(0F, (rect.height + ((paint.descent() + paint.ascent()) / 2)).toFloat(), paint, text)
        canvas?.restore()
    }

    override fun onDestroy() {}

    private fun spToPx(sp: Float, context: Context): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt()
    }

}