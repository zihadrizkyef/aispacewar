package com.zihadrizkyef.aispacewar.gameobject

import android.content.Context
import android.graphics.Color
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameroom.WarLand

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 08/10/18.
 */
class ShipGenerator(context: Context, room: GameRoom): GameObject(context, room) {

    private val generateShipRnbl = object:Runnable {
        override fun run() {
            generateRandomShip()

            val second = room.thread.MAX_FPS
            setAlarm(second * 1 + random.nextInt(1 * second), this)
        }
    }

    init {
        paint.color = Color.WHITE

        val ship = Ship(context, room)
        ship.size = 130
        ship.fireMax = 30.0
        ship.fireDelay = 3
        ship.speed = 9.0
        ship.paint.color = Color.GREEN
        room.addInstance(ship)

        for (i in 1..5) {
            generateRandomShip()
        }

        val second = room.thread.MAX_FPS
        setAlarm(random.nextInt(second * 1), generateShipRnbl)
    }

    private fun generateRandomShip(): Ship {
        val groupMemberLimit = if ((room as WarLand).shipList.any { it.size > 100 }) {
            5
        } else {
            3
        }

        var color = choose(
                Color.CYAN,
                Color.GREEN,
                Color.MAGENTA,
                Color.YELLOW,
                Color.RED
        )

        while (room.shipList.filter { it.paint.color == color }.size > groupMemberLimit) {
            color = choose(
                    Color.CYAN,
                    Color.GREEN,
                    Color.MAGENTA,
                    Color.YELLOW,
                    Color.RED
            )
        }

        val ship = Ship(context, room)
        ship.color = color
        ship.size = 20 + random.nextInt(20)
        val choosePlace = random.nextInt(4)
        when (choosePlace) {
            0 -> {
                ship.x = -ship.minSize.toFloat()
                ship.y = random.nextInt(room.height).toFloat()
            }
            1 -> {
                ship.x = random.nextInt(room.width).toFloat()
                ship.y = (room.height + ship.minSize).toFloat()
            }
            2 -> {
                ship.x = (room.width + ship.minSize).toFloat()
                ship.y = random.nextInt(room.height).toFloat()
            }
            3 -> {
                ship.x = random.nextInt(room.width).toFloat()
                ship.y = -ship.minSize.toFloat()
            }
        }
        room.addInstance(ship)
        return ship
    }

    override fun onUpdate() {
        super.onUpdate()

        if ((room as WarLand).shipList.any { it.size > 100 }) {
            if (room.shipList.size < 10) {
                generateRandomShip()
            }
        } else {
            if (room.shipList.size < 3) {
                generateRandomShip()
            }
        }
    }
}