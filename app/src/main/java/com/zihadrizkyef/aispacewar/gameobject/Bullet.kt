package com.zihadrizkyef.aispacewar.gameobject

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameroom.WarLand

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 10/10/18.
 */

class Bullet(val ship: Ship, context: Context, room: GameRoom) : GameObject(context, room) {
    private val paintText = Paint().apply { color = Color.WHITE }
    var color = ship.color

    init {
        speed = ship.size.toDouble() / 2
    }

    override fun onUpdate() {
        super.onUpdate()
        if ((x < 0 || x > room.width) || (y < 0 || y > room.height)) {
            room.removeInstance(this)
        }

        (room as WarLand).shipList.filter { it != ship }.forEach { enemy ->
            val intersectPoint = getCircleLineIntersectionPoint(
                    x.toDouble(),
                    y.toDouble(),
                    x + xLengthOfDir(speed, direction),
                    y + yLengthOfDir(speed, direction),
                    enemy.x.toDouble(),
                    enemy.y.toDouble(),
                    (enemy.size / 2).toDouble()
            )

            if (intersectPoint.isNotEmpty()) {
                if (enemy.color != color) {
                    room.removeInstance(this)
                    if (enemy.onCollisionWithBullet(this)) {
                        ship.onBulletKillEnemy()
                    }
                }
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.save()
        canvas?.translate(x, y)
        canvas?.rotate(-direction.toFloat())

        paint.color = color
        paint.alpha = 255
        canvas?.drawLine(0F, 0F, speed.toFloat(), 0F, paint)

        canvas?.restore()

        if (room.thread.DEBUG) {
            paint.color = color
            paint.alpha = 255
            val text = "x=$x\n" +
                    "y=$y\n" +
                    "dir=$direction\n" +
                    "spd=$speed\n"
            val xText = x
            var yText = y
            for (line in text.split("\n")) {
                canvas?.drawText(line, xText, yText, paintText)
                yText += paintText.descent() - paintText.ascent()
            }
        }
    }
}