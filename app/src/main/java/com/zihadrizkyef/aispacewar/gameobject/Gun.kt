package com.zihadrizkyef.aispacewar.gameobject

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/10/18.
 */
enum class Gun {
    SHOTGUN,
    SNIPER,
    SMG,
    RIFLE,
    MACHINEGUN
}