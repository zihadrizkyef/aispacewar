package com.zihadrizkyef.aispacewar.gameobject.item

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import com.zihadrizkyef.aispacewar.GameObject
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameobject.Bullet
import com.zihadrizkyef.aispacewar.gameobject.Ship
import com.zihadrizkyef.aispacewar.gameobject.ShipExplosion

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 08/10/18.
 */
class ItemGenerator(context: Context, room: GameRoom) : GameObject(context, room) {

    private val generateItemRnbl = object : Runnable {
        override fun run() {
            generateRandomItem()

            val second = room.thread.MAX_FPS
            setAlarm(second * 15 + random.nextInt(15 * second), this)

//            val second = room.thread.MAX_FPS
//            setAlarm(second * 1, this)
        }
    }

    init {
        paint.color = Color.WHITE

        val second = room.thread.MAX_FPS
        setAlarm(second * 15 + random.nextInt(15 * second), generateItemRnbl)

        //val second = room.thread.MAX_FPS
        //setAlarm(second * 1, generateItemRnbl)
    }

    private fun generateRandomItem(): Item {
        val item = choose(
                ItemSpeed(context, room),
                ItemSize(context, room),
                ItemFireRate(context, room),
                ItemFireMax(context, room)
        )
        item.x = random.nextInt(room.width).toFloat()
        item.y = random.nextInt(room.height / 2).toFloat()
        room.addInstance(item)

        val itemGenerateEffect = ItemGenerateEffect(context, room)
        itemGenerateEffect.x = item.x
        itemGenerateEffect.y = item.y
        itemGenerateEffect.paint.color = item.paint.color
        room.addInstance(itemGenerateEffect)

        return item
    }

    override fun onDraw(canvas: Canvas?) {
        if (room.thread.DEBUG) {
            canvas?.drawTextMultiline(10F, 10F, paint, "Ship Count : " + room.instanceList.filter { it is Ship }.size + "\n" +
                    "Bullet Count : " + room.instanceList.filter { it is Bullet }.size + "\n" +
                    "Explosion Count : " + room.instanceList.filter { it is ShipExplosion }.size + "\n")
        }
    }
}