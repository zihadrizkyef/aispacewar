package com.zihadrizkyef.aispacewar

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 11/10/18.
 */

class Function {
    val EPS = 0.0000001

    data class PointD(val x: Double, val y: Double)
    data class Circle(val x: Double, val y: Double, val r: Double)
    data class LineSegment(val x1: Double, val y1: Double, val x2: Double, val y2: Double)
    data class Line(val a: Double, val b: Double, val c: Double)
    data class Rectangle(var x: Double, var y: Double, var width: Double, var height: Double, var angle: Double)

    fun lineRectangleIntersection(pLine1: LineSegment, pLine2: LineSegment): PointD? {
        var result: PointD? = null

        val s1_x = pLine1.x2 - pLine1.x1
        val s1_y = pLine1.y2 - pLine1.y1

        val s2_x = pLine2.x2 - pLine2.x1
        val s2_y = pLine2.y2 - pLine2.y1

        val s = (-s1_y * (pLine1.x1 - pLine2.x1) + s1_x * (pLine1.y1 - pLine2.y1)) / (-s2_x * s1_y + s1_x * s2_y)
        val t = (s2_x * (pLine1.y1 - pLine2.y1) - s2_y * (pLine1.x1 - pLine2.x1)) / (-s2_x * s1_y + s1_x * s2_y)

        if (s in 0.0..1.0 && t >= 0 && t <= 1) {
            result = PointD((pLine1.x1 + t * s1_x), (pLine1.y1 + t * s1_y))
        }

        return result
    }

    fun circleRectangleIntersection(rect: Rectangle, circle: Circle): Boolean {
        val rectCenterX = rect.x + (rect.width / 2)
        val rectCenterY = rect.y + (rect.height / 2)
        val unrotatedCircleX = ((Math.cos(rect.angle) * (circle.x - rectCenterX) - Math.sin(rect.angle) * (circle.y - rectCenterY)) + rectCenterX)
        val unrotatedCircleY = (Math.sin(rect.angle) * (circle.x - rectCenterX) + Math.cos(rect.angle) * (circle.y - rectCenterY) + rectCenterY)
        val closestX = when {
            unrotatedCircleX < rect.x -> rect.x
            unrotatedCircleX > rect.x + rect.width -> rect.x + rect.width
            else -> unrotatedCircleX
        }
        val closestY = when {
            unrotatedCircleY < rect.y -> rect.y
            unrotatedCircleY > rect.y + rect.height -> rect.y + rect.height
            else -> unrotatedCircleY
        }

        val distance = GameObject.pointDistance(unrotatedCircleX.toFloat(), unrotatedCircleY.toFloat(), closestX.toFloat(), closestY.toFloat())
        return distance < circle.r
    }

    fun circleLineIntersection(circle: Circle, line: Line): ArrayList<PointD> {
        val a = line.a
        val b = line.b
        val c = line.c
        val x = circle.x
        val y = circle.y
        val r = circle.r

        val A = a * a + b * b
        val B = 2 * a * b * y - 2 * a * c - 2 * b * b * x
        val C = b * b * x * x + b * b * y * y - 2 * b * c * y + c * c - b * b * r * r

        var D: Double = (B * B - 4 * A * C)
        when {
            Math.abs(b) < EPS -> {
                val x1 = c / a

                // No intersection
                if (Math.abs(x - x1) > r) return arrayListOf()

                // Vertical line is tangent to circle
                if (Math.abs((x1 - r) - x) < EPS || Math.abs((x1 + r) - x) < EPS)
                    return arrayListOf(PointD(x1, y))

                val dx = Math.abs(x1 - x)
                val dy = Math.sqrt((r * r - dx * dx))

                // Vertical line cuts through circle
                return arrayListOf(PointD(x1, (y + dy)), PointD(x1, (y - dy)))
            }
            Math.abs(D) < EPS -> {

                val x1 = -B / (2 * A)
                val y1 = (c - a * x1) / b

                return arrayListOf(PointD(x1, y1))

            }
            D < 0 -> return arrayListOf()
            else -> {
                D = Math.sqrt(D)

                val x1 = (-B + D) / (2 * A)
                val y1 = (c - a * x1) / b

                val x2 = (-B - D) / (2 * A)
                val y2 = (c - a * x2) / b

                return arrayListOf(PointD(x1, y1), PointD(x2, y2))

            }
        }

    }

    // Converts a line segment to a line in general form
    fun segmentToGeneralForm(x1: Double, y1: Double, x2: Double, y2: Double): Line {
        val a = y1 - y2
        val b = x2 - x1
        val c = x2 * y1 - x1 * y2
        return Line(a, b, c)
    }

    // Chefuncks if a point 'pt' is inside the rect defined by (x1,y1), (x2,y2)
    private fun pointInRectangle(pt: PointD, x1: Double, y1: Double, x2: Double, y2: Double): Boolean {
        val x = Math.min(x1, x2)
        val X = Math.max(x1, x2)
        val y = Math.min(y1, y2)
        val Y = Math.max(y1, y2)
        return x - EPS <= pt.x && pt.x <= X + EPS &&
                y - EPS <= pt.y && pt.y <= Y + EPS
    }

    // Finds the intersection(s) of a line segment and a circle
    fun lineSegmentCircleIntersection(segment: LineSegment, circle: Circle): ArrayList<PointD> {

        val x1 = segment.x1
        val y1 = segment.y1
        val x2 = segment.x2
        val y2 = segment.y2
        val line = segmentToGeneralForm(x1, y1, x2, y2)
        val pts = circleLineIntersection(circle, line)

        // No intersection
        if (pts.isEmpty()) return arrayListOf()

        val pt1 = pts[0]
        val includePt1 = pointInRectangle(pt1, x1, y1, x2, y2)

        // Check for unique intersection
        if (pts.size == 1) {
            if (includePt1) return arrayListOf(pt1)
            return arrayListOf()
        }

        val pt2 = pts[1]
        val includePt2 = pointInRectangle(pt2, x1, y1, x2, y2)

        // Check for remaining intersections
        if (includePt1 && includePt2) return arrayListOf(pt1, pt2)
        if (includePt1) return arrayListOf(pt1)
        if (includePt2) return arrayListOf(pt2)
        return arrayListOf()

    }
}