package com.zihadrizkyef.aispacewar.gameroom

import android.content.Context
import android.util.AttributeSet
import android.view.SurfaceHolder
import com.zihadrizkyef.aispacewar.GameRoom
import com.zihadrizkyef.aispacewar.gameobject.Bullet
import com.zihadrizkyef.aispacewar.gameobject.Ship
import com.zihadrizkyef.aispacewar.gameobject.ShipGenerator
import com.zihadrizkyef.aispacewar.gameobject.StarBackground
import com.zihadrizkyef.aispacewar.gameobject.item.Item
import com.zihadrizkyef.aispacewar.gameobject.item.ItemGenerator

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 10/10/18.
 */

class WarLand @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : GameRoom(context, attrs, defStyleAttr), SurfaceHolder.Callback {
    var shipList = arrayListOf<Ship>()
    var itemList = arrayListOf<Item>()
    var bulletList = arrayListOf<Bullet>()

    override fun create() {
        addInstance(ShipGenerator(context, this))
        addInstance(ItemGenerator(context, this))
        addInstance(StarBackground(context, this))

//        addInstance(Ship(context, this).apply { x=100F; y=100F; speed})
//        addInstance(ItemSizeTest(context, this).apply { x=800F; y=200F; })
//        addInstance(ItemSize(context, this).apply { x=800F; y=200F; })
//        addInstance(ItemGenerator(context, this))
//        addInstance(StarBackground(context, this))
    }

    override fun update() {
        shipList.clear()
        shipList.addAll(instanceList.asSequence().filter { it is Ship }.map { it as Ship }.toList())

        itemList.clear()
        itemList.addAll(instanceList.asSequence().filter { it is Item }.map { it as Item }.toList())

        bulletList.clear()
        bulletList.addAll(instanceList.asSequence().filter { it is Bullet }.map { it as Bullet }.toList())

        super.update()
    }
}