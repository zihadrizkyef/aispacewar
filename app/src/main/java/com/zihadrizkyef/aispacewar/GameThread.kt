package com.zihadrizkyef.aispacewar

import android.graphics.Color
import android.util.Log
import android.view.SurfaceHolder

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 08/10/18.
 */
class GameThread(private val gameView: GameRoom, private val surfaceHolder: SurfaceHolder): Thread() {
    val DEBUG = false
    val MAX_FPS = 60
    private val NS_PER_FRAME = 1000_000_000/MAX_FPS
    var running = true

    private var totalTime = 0L
    private var totalFrame = 0

    override fun run() {
        while (running) {
            val startTime = System.nanoTime()
            val canvas = surfaceHolder.lockCanvas()
            synchronized(surfaceHolder) {
                if (canvas != null) {
                    gameView.update()
                    gameView.draw(canvas)
                } else {
                    Log.i("AOEU", "canvas null broh")
                }
            }
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas)
            } else {
                Log.i("AOEU", "canvas null lagi broh")
            }
            val timeNanosTaken = System.nanoTime()-startTime
            val sleepTime = NS_PER_FRAME-timeNanosTaken
            if (sleepTime>0) { sleep(sleepTime/1000_000) }

            totalTime += System.nanoTime()-startTime
            totalFrame++

            if (totalFrame >= MAX_FPS) {
                val fps = 1000_000_000/(totalTime/totalFrame)
                Log.i("AOEU", "FPS : $fps")
                totalFrame = 0
                totalTime = 0L
            }
        }
    }
}